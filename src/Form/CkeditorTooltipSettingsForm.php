<?php

namespace Drupal\ckeditor_tippy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CkeditorTooltipSettingsForm.
 *
 * @package Drupal\ckeditor_tippy\Form
 */
class CkeditorTooltipSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckeditor_tippy_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['ckeditor_tippy.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ckeditor_tippy.settings');

    $form['settings'] = [
      '#type' => 'html_tag',
      '#tag' => 'tippy',
      '#value' => $this->t('This text is an example of text with tooltip.<br/>Hover after the save to see the new configuration. <br/>
Another new line. Useful to test \'follow cursor\' method on Y axis.'),
      '#attributes' => [
        'data-tippy-content' => 'hello',
        'class' => [
          'tippy-tooltip-text',
        ],
      ],
    ];
    $form['follow_cursor'] = [
      '#type' => 'radios',
      '#title' => $this->t('Follow cursor'),
      '#description' => $this->t("Determines if the tippy follows the user's mouse cursor."),
      '#default_value' => $config->get('follow_cursor') ?: 0,
      '#options' => [
        FALSE => $this->t('Default'),
        'initial' => $this->t('Initial'),
        TRUE => $this->t('Follow on both x and y axes'),
        'horizontal' => $this->t('Horizontal - follow on x axis'),
        'vertical' => $this->t('Vertical - follow on y axis'),
      ],
      '#return_value' => TRUE,
    ];

    $form['prevent_overflow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent overflow'),
      '#description' => $this->t('Modifier used to prevent the popper from being positioned outside the boundary'),
      '#return_value' => TRUE,
      '#default_value' => $config->get('prevent_overflow') ?: FALSE,
    ];

    $form['interactive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Interactive tooltip'),
      '#description' => $this->t('Determines if the tippy has interactive content inside of it, so that it can be hovered over and clicked inside without hiding.'),
      '#return_value' => TRUE,
      '#default_value' => $config->get('interactive') ?: FALSE,
    ];

    $form['load_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load Popper.js library from CDN'),
      '#description' => $this->t('Loads Popper.js library from CDN if checked or from local system if unchecked'),
      '#return_value' => TRUE,
      '#default_value' => $config->get('load_cdn') === NULL ? TRUE : $config->get('load_cdn'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ckeditor_tippy.settings');
    $values = $form_state->getValues();

    $config->set('follow_cursor', $values['follow_cursor']);
    $config->set('prevent_overflow', $values['prevent_overflow']);
    $config->set('interactive', $values['interactive']);
    $config->set('load_cdn', $values['load_cdn']);

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
