<?php

/**
 * @file
 * Allows users to create an tippyjs tooltip using CKEditor.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_page_attachments_alter().
 */
function ckeditor_tippy_page_attachments_alter(array &$page) {
  $config = \Drupal::config('ckeditor_tippy.settings');

  $page['#attached']['library'][] = 'ckeditor_tippy/tippyjs';
  $page['#attached']['drupalSettings']['ckeditor_tippy'] = [
    'followCursor' => $config->get('follow_cursor') ?: FALSE,
    'interactive' => $config->get('interactive') ?: FALSE,
  ];
  if (!empty($config->get('prevent_overflow'))) {
    $page['#attached']['drupalSettings']['ckeditor_tippy']['popperOptions']['modifiers'][] = [
      'name' => 'preventOverflow',
      'options' => [
        'altAxis' => $config->get('prevent_overflow'),
      ],
    ];
  }
}

/**
 * Implements hook_form_FORM_BASE_ID_alter().
 */
function ckeditor_tippy_form_editor_image_dialog_alter(&$form, FormStateInterface $form_state, $form_id) {
  /** @var \Drupal\editor\EditorInterface $editor */
  $user_input = $form_state->getUserInput();
  $input = isset($user_input['editor_object']) ? $user_input['editor_object'] : [];
  $form['title'] = [
    '#title' => t('Image title'),
    '#description' => t('Displayed as tooltip when mouse is hovering the image.'),
    '#default_value' => $input['data-tippy-content'] ? $input['data-tippy-content'] : '',
    '#type' => 'textfield',
    '#required' => FALSE,
    '#maxlength' => 2048,
    '#parents' => ['attributes', 'data-tippy-content'],
  ];
  $form['actions']['save_modal']['#validate'][] = 'image_form_editor_image_dialog_validate';
}

/**
 * Validation callback for form_editor_image_dialog_form.
 */
function image_form_editor_image_dialog_validate(array &$form, FormStateInterface $form_state) {
  if (empty($form_state->getValue('fid')[0])) {
    return;
  }
  $attributes = $form_state->getValue('attributes');
  if (!$attributes) {
    return;
  }
  $form_state->setValue('attributes', $attributes);
}

/**
 * Implements hook_library_info_alter().
 */
function ckeditor_tippy_library_info_alter(&$libraries, $extension) {
  $load_cdn = \Drupal::config('ckeditor_tippy.settings')->get('load_cdn');
  if ($extension === 'ckeditor_tippy' && isset($libraries['tippyjs']) && !$load_cdn) {
    unset($libraries['tippyjs']['js']['//unpkg.com/@popperjs/core@2.11.8/dist/umd/popper.min.js']);
    
    $libraries['tippyjs']['js']['js/popper.min.js'] = [
      'type' => 'file',
      'weight' => -10,
    ];
  }
}