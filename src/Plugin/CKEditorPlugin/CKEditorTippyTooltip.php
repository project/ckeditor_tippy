<?php

namespace Drupal\ckeditor_tippy\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "CKEditorTippyTooltip" plugin.
 *
 * @CKEditorPlugin(
 *   id = "tippy_tooltip",
 *   label = @Translation("CKEditor Tooltip"),
 *   module = "ckeditor_tippy"
 * )
 */
class CKEditorTippyTooltip extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
   */
  public function getFile() {
    return \Drupal::moduleHandler()->getModule('ckeditor_tippy')->getPath() . '/js/plugins/ckeditor_tippy/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = \Drupal::moduleHandler()->getModule('ckeditor_tippy')->getPath();
    $path = $module_path . '/js/plugins/ckeditor_tippy/icons/';
    return [
      'tippy_tooltip' => [
        'label' => $this->t('Add Tooltip'),
        'image' => $path . 'tooltip.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'drupalTooltip_dialogTitleAdd' => $this->t('Add Tooltip'),
      'drupalTooltip_dialogTitleEdit' => $this->t('Edit Tooltip'),
    ];
  }

}
