# CKEditor Tippy

This module adds a CKEditor button that enables users to add tooltips to text or images. The tooltip functionality is based on Tippy.js (https://atomiks.github.io/tippyjs/).

## Requirements

If your text format has the `Limit allowed HTML tags and correct faulty HTML` filter enabled, add this bit to the allowed HTML tags:

`<tippy data-tippy-content class="tippy-tooltip-text">`
`<img data-tippy-content>`
