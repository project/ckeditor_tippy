<?php

namespace Drupal\ckeditor_tippy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;

/**
 * Provides a link dialog for text editors.
 *
 * @internal
 */
class TooltipEditorDialog extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editor_tooltip_dialog';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    // The default values are set directly from \Drupal::request()->request,
    // provided by the editor plugin opening the dialog.
    $user_input = $form_state->getUserInput();
    $input = isset($user_input['editor_object']) ? $user_input['editor_object'] : [];

    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="editor-tooltip-dialog-form">';
    $form['#suffix'] = '</div>';

    $form['attributes']['body_text'] = [
      '#title' => ($user_input['editor_object']['text']) ? $this->t('Selected text') : $this->t('Insert new text'),
      '#type' => 'textfield',
      '#default_value' => ($user_input['editor_object']['text']) ? $user_input['editor_object']['text'] : '',
      '#disabled' => !empty($user_input['editor_object']['text']) ? TRUE : FALSE,
      '#maxlength' => 2048,
      '#description' => ($user_input['editor_object']['text']) ? $this->t('Tooltip is added to this text.') : $this->t('Insert new text to be added at the current cursor position.'),
    ];

    // Everything under the "attributes" key is merged directly into the
    // generated link tag's attributes.
    $form['attributes']['tooltip_text'] = [
      '#title' => $this->t('Tooltip text'),
      '#type' => 'textfield',
      '#default_value' => isset($input['data-tippy-content']) ? $input['data-tippy-content'] : '',
      '#maxlength' => 2048,
      '#description' => $this->t('Enter the text which appears on mouse hover. Leave empty to remove the tooltip.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      // No regular submit-handler. This form only works via JavaScript.
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-tooltip-dialog-form', $form));
    }
    else {
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

}
