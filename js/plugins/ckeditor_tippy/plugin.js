(function ($, Drupal, drupalSettings, CKEDITOR) {
  CKEDITOR.addCss( 'tippy.tippy-tooltip-text { outline: 1px solid #ffd25c; border: 1px solid #ffd25c; } ' );

  function parseAttributes(editor, element) {
    var parsedAttributes = {};

    var domElement = element.$;
    var attribute = void 0;
    var attributeName = void 0;

    for (var attrIndex = 0; attrIndex < domElement.attributes.length; attrIndex++) {
      attribute = domElement.attributes.item(attrIndex);
      attributeName = attribute.nodeName.toLowerCase();

      parsedAttributes[attribute.nodeName] = element.data('tippy-content') || attribute.nodeValue;
    }

    parsedAttributes['text'] = element.getText();
    return parsedAttributes;
  }

  var registeredTooltipWidgets = [];

  function registerTooltipWidget(widgetName) {
    registeredTooltipWidgets.push(widgetName);
  }

  function getFocusedTooltipWidget(editor) {
    var widget = editor.widgets.focused;
    if (widget && registeredTooltipWidgets.indexOf(widget.name) !== -1) {
      return widget;
    }
    return null;
  }

  function getSelectedTooltip(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();

    if (selectedElement && selectedElement.is('tippy') && selectedElement.hasClass('tippy-tooltip-text')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];
    if (range) {
      range.shrink(CKEDITOR.SHRINK_TEXT);
      return editor.elementPath(range.getCommonAncestor()).contains('tippy', 1);
    }

    return null;
  }

  CKEDITOR.plugins.add('tippy_tooltip', {
    requires: 'widget,dialog',
    icons: 'tippytooltip',
    hidpi: true,
    init: function init(editor) {
      // Add widget.
      editor.ui.addButton('tippy_tooltip', {
        label: "Add Tooltip",
        command: 'tippy_tooltip',
        icon: this.path + 'icons/tooltip.png'
      });

      editor.addCommand('tippy_tooltip', {
        dialog: 'tippy_tooltip',
        init: function () {},
        template: '<a class="tippy-tooltip-text" data-tippy-content=""></a>',
        data: function () {
          var $el = jQuery(this.element.$);
          // if (!this.data.text) {
          //   return;
          // }
          $el.attr('data-tippy-content', this.data.tooltip);
          $el.text(this.data.text);
          $el.attr('tooltip_text', this.data.text);
        },

        allowedContent: 'tippy[data-tippy-content](tippy-tooltip-text)',
        requiredContent: 'tippy[data-tippy-content](tippy-tooltip-text)',

        upcast: function (element) {
          return element.name == 'tippy' && el.hasClass('tippy-tooltip-text');
        },
        exec: function exec(editor) {

          var focusedTooltipWidget = getFocusedTooltipWidget(editor);
          var tooltipElement = getSelectedTooltip(editor);

          var existingValues = {};
          if (tooltipElement && tooltipElement.$) {
            existingValues = parseAttributes(editor, tooltipElement);
          }
          else if (focusedTooltipWidget && focusedTooltipWidget.data.tooltip_text) {
            existingValues = CKEDITOR.tools.clone(focusedTooltipWidget.data.tooltip_text);
          }

          var saveCallback = function saveCallback(returnValues) {
            if (focusedTooltipWidget) {
              focusedTooltipWidget.setData('tooltip', CKEDITOR.tools.extend(returnValues.attributes, focusedTooltipWidget.data.tooltip_text));
              editor.fire('saveSnapshot');
              return;
            }

            editor.fire('saveSnapshot');

            if (tooltipElement && !returnValues.attributes.tooltip_text) {
              var new_el = CKEDITOR.dom.element.createFromHtml(tooltipElement.getText());
              new_el.replace(tooltipElement);
              return;
            }
            else if (!tooltipElement) {
              var selection = editor.getSelection();
              var range = selection.getRanges(1)[0];

              var style = new CKEDITOR.style({
                element: 'tippy',
                attributes: {
                  class: 'tippy-tooltip-text',
                  'data-tippy-content': returnValues.attributes['tooltip_text']
                }
              });
              style.type = CKEDITOR.STYLE_INLINE;
              style.applyToRange(range);

              range.select();

              tooltipElement = getSelectedTooltip(editor);
              if (returnValues.attributes['body_text'] != '') {
                tooltipElement.$.innerHTML = returnValues.attributes['body_text'];
              }
            }
            else if (tooltipElement) {
              tooltipElement.data('tippy-content', returnValues.attributes['tooltip_text']);
            }
          };

          var bodyText = editor.getSelectedHtml().getHtml();
          if (!existingValues.text && bodyText != '') {
            existingValues = {
              'class': 'tooltip_text',
              'text': (new DOMParser).parseFromString(bodyText, "text/html").documentElement.textContent,
            };
          }

          var dialogSettings = {
            title: tooltipElement ? editor.config.drupalTooltip_dialogTitleEdit : editor.config.drupalTooltip_dialogTitleAdd,
            dialogClass: 'editor-tooltip-dialog'
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url('ckeditor_tippy/dialog/tippy/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);

        },
      });
      editor.on('doubleclick', function (evt) {
        var element = getSelectedTooltip(editor) || evt.data.element;
        if (!element.isReadOnly()) {
          if (element.is('tippy') && element.hasClass('tippy-tooltip-text')) {
            editor.getSelection().selectElement(element);
            editor.getCommand('tippy_tooltip').exec();
          }
        }
      });

      if (editor.addMenuItems) {
        editor.addMenuItems({
          label: Drupal.t('Edit Tooltip'),
          command: 'tippy_tooltip',
          order: 1
        });
      }
    }
  });

  CKEDITOR.plugins.tippy_tooltip = {
    registerLinkableWidget: registerTooltipWidget
  };
})(jQuery, Drupal, drupalSettings, CKEDITOR);

