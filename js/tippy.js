(function ($, Drupal, drupalSettings, _) {

  'use strict';

  Drupal.behaviors.tippy_tooltip = {
    attach: function (context) {
      $(document).ready(function () {
       tippy('[data-tippy-content]', drupalSettings.ckeditor_tippy);
      });
    }
  };
}(jQuery, Drupal, drupalSettings, ));