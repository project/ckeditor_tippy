(function (CKEDITOR) {
  function findElementByName(element, name) {
    if (element.name === name) {
      return element;
    }

    var found = null;
    element.forEach(function (el) {
      if (el.name === name) {
        found = el;

        return false;
      }
    }, CKEDITOR.NODE_ELEMENT);
    return found;
  }

  CKEDITOR.plugins.add('drupalimagetippy', {
    requires: 'drupalimage',

    beforeInit: function beforeInit(editor) {
      editor.on('widgetDefinition', function (event) {
        var widgetDefinition = event.data;
        if (widgetDefinition.name !== 'image') {
          return;
        }

        CKEDITOR.tools.extend(widgetDefinition.features, {
          ckeditor_tippy: {
            requiredContent: 'img[data-tooltip-content]'
          }
        }, true);

        var requiredContent = widgetDefinition.requiredContent.getDefinition();
        requiredContent.attributes['data-tippy-content'] = '';
        widgetDefinition.requiredContent = new CKEDITOR.style(requiredContent);
        widgetDefinition.allowedContent.img.attributes['!data-tippy-content'] = true;

        var originalDowncast = widgetDefinition.downcast;
        widgetDefinition.downcast = function (element) {
          var img = findElementByName(element, 'img');
          originalDowncast.call(this, img);

          var attrs = img.attributes;

          if (!!this.data['data-tippy-content']) {
            attrs['data-tippy-content'] = this.data['data-tippy-content'];
          }

          if (img.parent.name === 'a') {
            return img.parent;
          }

          return img;
        };

        var originalUpcast = widgetDefinition.upcast;
        widgetDefinition.upcast = function (element, data) {
          if (element.name !== 'img' || !element.attributes['data-entity-type'] || !element.attributes['data-entity-uuid']) {
            return;
          }

          if (element.attributes['data-cke-realelement']) {
            return;
          }

          element = originalUpcast.call(this, element, data);
          var attrs = element.attributes;

          if (element.parent.name === 'a') {
            element = element.parent;
          }

          if (element.name !== 'img') {
            var img = element.find('img');
            if (img.length === 0) {
              return element;
            }

            attrs = img[0].attributes;
          }

          data['data-tippy-content'] = attrs['data-tippy-content'];

          return element;
        };

        CKEDITOR.tools.extend(widgetDefinition._mapDataToDialog, {
          'data-tippy-content': 'data-tippy-content',
        });
      }, null, null, 20);
    }
  });
})(CKEDITOR);